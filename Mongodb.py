""" En base a la aplicación desarrollada en la Sustitución de SQLite por SQLAlchemy,
 debe sustituir el uso de bases de datos relacionales por el uso de base de datos
 no relacionales, específicamente MongoDB."""
import sys
from pymongo import MongoClient

MONGO_URI ='mongodb:///localhost'

client = MongoClient(MONGO_URI)

db = client['slangoriginal']
collection = db['vocabulario']

def slang_panameno():
    palabra = input("Digite la palabra del slang panameño: ")
    definicion = input("Digite la definicion de la palabra: ")
    dic = {"palabra": palabra, "definicion": definicion}
    collection.insert_one(dic)
    print("La palabra fue agregada exitosamente")
    opciones()

def editar_palabra():
    print("Las palabras que pueden ser modificadas son: ")
    busq = collection.find({},{"_id":0, "definicion":0})
    for resultado in busq:
        print(resultado)
    palabra=input("Escriba la palabra que desea editar: ")
    diccionari=1
    busqueda = collection.find({"palabra":palabra}, {"_id":0, "definicion":0})
    for resultado in busqueda:
        diccionari=0
        print(f"La palabra a modificar es: {resultado}")
    if diccionari==1:
        print(f"La palabra '{palabra}' no existe")
    else:
        palabraeditada=input("Escriba la nueva palabra: ")
        collection.update_one({"palabra":palabra},{"$set":{"palabra": palabraeditada}})
        print("La palabra se ha editado corectamente")
        opciones()

def eliminar_palabra():
    print("Las palabras que pueden ser eliminadas son: ")
    busq = collection.find({}, {"_id": 0, "definicion": 0})
    for resultado in busq:
        print(resultado)
    palabra = input("Escriba la palabra que desea eliminar: ")
    diccionari = 1
    busqueda = collection.find({"palabra": palabra}, {"_id": 0, "definicion": 0})
    for resultado in busqueda:
        diccionari = 0
        print(f"La palabra a eliminar es: {resultado}")
    if diccionari == 1:
        print(f"La palabra '{palabra}' no existe")
    else:
        collection.delete_one({"palabra": palabra})
        print("La palabra se ha eliminado corectamente")
        opciones()

def ver_palabras():
    busq = collection.find({}, {"_id": 0})
    for resultado in busq:
        print(resultado)
    opciones()

def buscar_significado():
    palabra = input ("Escriba la palabra que desea buscar: ")
    busqueda = collection.find({"palabra":palabra},{"_id":0})
    diccionari = 1
    for resultado in busqueda:
        diccionari = 0
        print(resultado)
    if diccionari == 1:
        print(f"La palabra '{palabra}' no existe")
    opciones()

def opciones():
    print("Bienvenidos al slang panemeño en la base de datos Mongodb")
    print("1- Agregar nueva palabra")
    print("2- Editar palabra existente")
    print("3- Eliminar palabra existente")
    print("4- Ver listado de palabras")
    print("5- Buscar significado de palabras")
    print("6- Salir")
    opcion=input("Seleccione una opcion:")
    if opcion=='1':
        slang_panameno()
    elif opcion == '2':
        editar_palabra()
    elif opcion=='3':
        eliminar_palabra()
    elif opcion=='4':
        ver_palabras()
    elif opcion=='5':
         buscar_significado()
    elif opcion=='6':
        print("Regrese pronto")
        sys.exit()
    else:
        input("Seleccione una opcion valida !!!")
        opciones()
opciones()
